package com.sjostric.spark.hello.repostitory;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoDbRepository {

    protected DB getDbConnection(String dbName) throws Exception {
        MongoClient mongoClient = new MongoClient();
        return mongoClient.getDB(dbName);
    }

}
