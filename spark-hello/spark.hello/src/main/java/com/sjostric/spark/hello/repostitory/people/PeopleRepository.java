package com.sjostric.spark.hello.repostitory.people;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sjostric.spark.hello.repostitory.MongoDbRepository;

public class PeopleRepository extends MongoDbRepository {

    private DB db;

    public PeopleRepository(
        String dbName) {
        try {
            this.db = getDbConnection(dbName);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to create db conn: "
                + e.getMessage(), e);
        }
    }

    public DBObject getPerson(String name) {
        DBCollection collection = db.getCollection("people");
        BasicDBObject query = new BasicDBObject("name", name);
        BasicDBObject projection = new BasicDBObject("_id", false);
        return collection.findOne(query, projection);
    }

    public List<DBObject> getPersons() {
        DBCollection collection = db.getCollection("people");
        BasicDBObject query = new BasicDBObject();
        BasicDBObject projection = new BasicDBObject("_id", false);
        DBCursor cursor = collection.find(query, projection);
        List<DBObject> people = new ArrayList<DBObject>();
        try {
            while (cursor.hasNext()) {
                people.add(cursor.next());
            }
        } finally {
            cursor.close();
        }
        return people;
    }

}
