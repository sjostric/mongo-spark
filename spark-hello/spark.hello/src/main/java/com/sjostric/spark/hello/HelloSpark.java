package com.sjostric.spark.hello;

import static spark.Spark.get;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import spark.Request;
import spark.Response;
import spark.Route;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.DBObject;
import com.sjostric.spark.hello.repostitory.people.PeopleRepository;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class HelloSpark {

    private String dbName = "test";

    private static Configuration cfg;
    static {
        cfg = new Configuration(Configuration.VERSION_2_3_22);
        cfg.setClassForTemplateLoading(HelloSpark.class, "/");
        cfg.setDefaultEncoding("UTF-8");
    }

    private PeopleRepository peopleRepo;

    public static void main(String[] args) {
        new HelloSpark().start();
    }

    public HelloSpark() {
        peopleRepo = new PeopleRepository(dbName);
    }

    private void start() {

        get("/", new Route() {
            @Override
            public Object handle(Request request, Response response)
                    throws Exception {
                Map<String, Object> saying = new HashMap<>();
                saying.put("saying", "WORLD");
                return processTemplate(saying, "hellospark.ftl");
            }

        });

        get("/hello", new Route() {
            @Override
            public Object handle(Request request, Response response) {
                MyData myData = new MyData("Hello", "World");
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                return gson.toJson(myData);
            }

        });

        get("/people", new Route() {

            @Override
            public Object handle(Request request, Response response)
                    throws Exception {
                List<DBObject> people = peopleRepo.getPersons();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                return gson.toJson(people);
            }

        });

        get("/people/:name", new Route() {

            @Override
            public Object handle(Request request, Response response)
                    throws Exception {
                DBObject person = peopleRepo.getPerson(request.params(":name"));
                return processTemplate(person.toMap(), "people.ftl");
            }

        });

        get("/people/:name/json", new Route() {

            @Override
            public Object handle(Request request, Response response)
                    throws Exception {
                DBObject person = peopleRepo.getPerson(request.params(":name"));
                Gson gson = new Gson();
                return gson.toJson(person);
            }

        });

    }

    private Object processTemplate(Map<String, Object> valueMap,
            String templateName) throws TemplateNotFoundException,
            MalformedTemplateNameException, ParseException, IOException,
            TemplateException {
        Template template = cfg.getTemplate(templateName);
        Writer out = new StringWriter();
        template.process(valueMap, out);
        return out;
    }

}

class MyData {
    private String saying;
    private String to;

    public MyData(
        String saying, String to) {
        this.saying = saying;
        this.to = to;
    }
}
